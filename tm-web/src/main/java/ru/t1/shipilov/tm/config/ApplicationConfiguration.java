package ru.t1.shipilov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.shipilov.tm")
public class ApplicationConfiguration {
}
