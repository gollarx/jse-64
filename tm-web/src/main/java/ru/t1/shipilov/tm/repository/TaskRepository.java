package ru.t1.shipilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.shipilov.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    @NotNull
    private static final TaskRepository INSTANCE = new TaskRepository();

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    @NotNull
    private Map<String, Task> tasks = new LinkedHashMap<>();

    private TaskRepository() {
    }

    {
        add(new Task("Task 1"));
        add(new Task("Task 2"));
        add(new Task("Task 3"));
    }

    public void create() {
        add(new Task("New Task " + System.currentTimeMillis()));
    }

    public void add(@NotNull Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull String id) {
        tasks.remove(id);
    }

}
