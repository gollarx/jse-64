package ru.t1.shipilov.tm.exception.entity.field;

public class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! Status is empty...");
    }

}
