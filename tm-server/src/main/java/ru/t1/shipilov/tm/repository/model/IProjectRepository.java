package ru.t1.shipilov.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.shipilov.tm.model.Project;

@Repository
public interface IProjectRepository extends IUserOwnedRepository<Project> {
}

